"""Tests for the count sketch submodule."""

import pytest
from stream.count_sketch import CountSketch

INPUT_AND_ESTIMATE = [(1, 1), (2, 1), (1, 2), (3, 1), (2, 2), (2, 3), (3, 2), (1, 3)]

COUNT_SKETCH_FOR_DEPENDENT_TESTS = CountSketch(100, 100, 1024)

@pytest.mark.parametrize("input_and_estimate", INPUT_AND_ESTIMATE)
def test_update_and_estimate(input_and_estimate):
    """Test update and estimate methods."""
    COUNT_SKETCH_FOR_DEPENDENT_TESTS.update(input_and_estimate[0], 1)
    assert COUNT_SKETCH_FOR_DEPENDENT_TESTS.estimate(input_and_estimate[0]) == \
        input_and_estimate[1]
