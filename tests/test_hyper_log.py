"""Tests for the bloom filter submodule."""

import random
import pytest
from stream.hyper_log import HyperLogLog

DOMAIN_AND_ERROR_LARGE = [(128, 100), (256, 200), (512, 400), (1028, 800)]
DOMAIN_AND_ERROR_SMALL = [(4, 10), (8, 20), (16, 40), (32, 80)]
DOMAIN_WHEN_ESTIMATE_NOT_NEEDED = [(128, 240)]

@pytest.mark.parametrize("domain_and_error", DOMAIN_AND_ERROR_LARGE)
def test_update_and_estimate_without_correct(domain_and_error):
    """Test update and estimate without correction."""
    hyper_log_log_for_tests = HyperLogLog(64, 1024)
    for _ in range(1000000):
        hyper_log_log_for_tests.update(random.randint(0, domain_and_error[0]))
    assert hyper_log_log_for_tests.estimate() < \
        domain_and_error[0] + domain_and_error[1]
    assert hyper_log_log_for_tests.estimate() > \
        domain_and_error[0] - domain_and_error[1] 

@pytest.mark.parametrize("domain_and_error", DOMAIN_AND_ERROR_SMALL)
def test_update_and_estimate_and_correct(domain_and_error):
    """Test update and estimate with correction.""" 
    hyper_log_log_with_correct_for_tests = HyperLogLog(64, 1024)
    for _ in range(1000000):
        hyper_log_log_with_correct_for_tests.\
            update(random.randint(0, domain_and_error[0]))
    estimate = hyper_log_log_with_correct_for_tests.estimate() 
    correction = hyper_log_log_with_correct_for_tests.correct(estimate)
    assert correction < domain_and_error[0] + domain_and_error[1]
    assert correction > domain_and_error[0] - domain_and_error[1]

@pytest.mark.parametrize("domain_and_error", DOMAIN_WHEN_ESTIMATE_NOT_NEEDED)
def test_update_and_estimate_and_correct_not_needed(domain_and_error):
    """Test update and estimate with correction when correction not needed."""
    hyper_log_log_without_correct_for_tests = HyperLogLog(4, 1024)
    for _ in range(1000000):
        hyper_log_log_without_correct_for_tests.\
            update(random.randint(0, domain_and_error[0]))
    estimate = hyper_log_log_without_correct_for_tests.estimate()
    with pytest.raises(Exception):
        correction = hyper_log_log_without_correct_for_tests.correct(estimate)
