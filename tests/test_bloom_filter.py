"""Tests for the bloom filter submodule."""

import pytest
from stream.bloom_filter import BloomFilter

INPUT_AND_ESTIMATE = [(1, 1), (2, 1), (1, 2), (3, 1), (2, 2), (2, 3), (3, 2), (1, 3)]

BLOOM_FILTER_FOR_DEPENDENT_TESTS = BloomFilter(100, 5, 1024)

@pytest.mark.parametrize("input_and_estimate", INPUT_AND_ESTIMATE)
def test_update_and_estimate(input_and_estimate):
    """Test update and estimate methods."""
    BLOOM_FILTER_FOR_DEPENDENT_TESTS.update(input_and_estimate[0], 1)
    assert BLOOM_FILTER_FOR_DEPENDENT_TESTS.estimate(input_and_estimate[0]) == \
        input_and_estimate[1]
