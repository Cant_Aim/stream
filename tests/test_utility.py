"""Tests for the stream utility module."""
import pytest
from stream.utility import Utility

PRIMES_GIVEN_INPUT = [(1, 30, 10, [1, 3, 5, 7, 11, 13, 17, 19, 23, 29]),
                      (5, 30, 3, [5, 7, 11]),
                      (10, 20, 20, [11, 13, 17, 19])]

PRIMES_RSA_GIVEN_INPUT = [(1024, 20, 512), (2048, 40, 1024)]

UNIVERSAL_HASH_INPUT = [(10, 5, [7, 4, 5, 6, 2]),
                        (5, 2, [1, 2, 3, 4, 5]),
                        (100, 3, [99, 43, 23])]

@pytest.fixture
def utility_refresh():
    """Fixture for creating fresh CountSketch instance."""
    return Utility()

@pytest.mark.parametrize("input_and_result", PRIMES_GIVEN_INPUT)
def test_primes(utility_refresh, input_and_result):
    """Test primes generation using simple method."""
    primes = utility_refresh.primes(input_and_result[0],
                                    input_and_result[1],
                                    input_and_result[2])
    assert primes == input_and_result[3]


@pytest.mark.parametrize("input_and_result", PRIMES_RSA_GIVEN_INPUT)
def test_primes_rsa(utility_refresh, input_and_result):
    """Test primes generation using rsa method."""
    primes = utility_refresh.primes_rsa(input_and_result[0],
                                        input_and_result[1])
    assert len(primes) == input_and_result[1]
    assert len(bin(primes[0])[2:]) == input_and_result[2]

@pytest.mark.parametrize("input_and_result", UNIVERSAL_HASH_INPUT)
def test_universal_hash_generation(utility_refresh, input_and_result):
    """Test universal hash function family that is generated."""
    hash_function = utility_refresh.universal_hash_functions(input_and_result[0],
                                                             input_and_result[1],
                                                             utility_refresh.primes)
    for i in range(input_and_result[1]):
        for x in input_and_result[2]:
            assert hash_function(x, i, 2) <= 2
            assert hash_function(x, i, 2) >= 0

@pytest.mark.parametrize("input_and_result", UNIVERSAL_HASH_INPUT)
def test_sign_hash_functions(utility_refresh, input_and_result):
    """Test sign function that is derived from universal hash function."""
    hash_function = utility_refresh.universal_hash_functions(input_and_result[0],
                                                             input_and_result[1],
                                                             utility_refresh.primes)
    sign_function = utility_refresh.sign_hash_functions(hash_function)
    for i in range(input_and_result[1]):
        for x in input_and_result[2]:
            assert sign_function(x, i) <= 1
            assert sign_function(x, i) >= -1
