"""
Setup file for the stream package, used for local installation.
"""
import io

from setuptools import find_packages, setup

with io.open("README.md", "rt", encoding="utf8") as f:
    README = f.read()

setup(
    name="stream",
    version="0.0.1",
    url="https://gitlab.com/Cant_Aim",
    license="gplv3",
    maintainer="Ivan Pozdnyakov",
    maintainer_email="ivan.v.pozdnyakov@gmail.com",
    description="Probabilistic data structures for streaming problems",
    long_description=README,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "pycryptodome",
    ],
    extras_require={
        "test": [
            "pytest",
            "coverage",
        ],
    },
)
