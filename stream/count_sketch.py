"""
This is a module for count sketch primitive.
"""

import statistics
from stream.utility import Utility

class CountSketch:
    """
    A count sketch that instantiates the necessary base matrix,
    hash functions for assigning to columns along each row and
    binary hash function for assigning keys to -1 or 1.

    .. code-block:: text

        *------------------*     for each row we have hash function
        |  |  |  |  |  |   |     that decides column a key goes into
        *------------------*     and an addigional sign hash that
        |  |  |  |  |  |   |     says whether we will increment counter
        *------------------*     in cell by 1 or -1.
        |  |  |  |  |  |   |
        *------------------*

    Parameters:
        - buckets (int): number of buckets to hash to.
        - hashs (int): number of rows, will generate hash for each.
        - max key (int): maximum expected size for a key.

    Return:
        - Instance of CountSketch.
    """
    def __init__(self, buckets: int, hashs: int, max_key: int):
        self.utility = Utility()
        self.matrix = [[0 for _ in range(hashs)] for _ in range(buckets)]
        self.hash_bucket_funcs = self.utility.universal_hash_functions(max_key,
                                                                       hashs,
                                                                       self.utility.primes_rsa)
        self.hash_sign_funcs = self.utility.sign_hash_functions(\
                                self.utility.universal_hash_functions(max_key,
                                                                      hashs,
                                                                      self.utility.primes_rsa))


    def update(self, key: int, value: int = 1):
        """
        Basic primitive for updating sketch matrix.

        Parameters:
            - key (int): item in stream to process.
            - value (int): multiplicative value associated key

        Return:
            - None.
        """
        for i in range(len(self.matrix[0])):
            bucket = self.hash_bucket_funcs(key, i, len(self.matrix))
            self.matrix[bucket][i] += self.hash_sign_funcs(key, i)*value

    def estimate(self, key: int):
        """
        Basic primitive for estimating value in current sketch matrix.

        Parameters:
            - key (int): item for which estimate frequency.

        Return:
            - None.
        """
        values = []
        for i in range(len(self.matrix[0])):
            bucket = self.hash_bucket_funcs(key, i, len(self.matrix))
            values.append(self.matrix[bucket][i]*self.hash_sign_funcs(key, i))
        return statistics.median(values)
