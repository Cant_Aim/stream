"""
This is a module for hyperloglog primitive.
"""

import math
from stream.utility import Utility

class HyperLogLog:
    """
    A hyperloglog class that instantiates the necessary counting array
    and the hash function for assigning input to array indices.

    Parameters:
        - buckets (int): number of buckets to hash to.
        - max key (int): maximum expected size for a key.

    Return:
        - Instance of HyperLogLog.
    """
    def __init__(self, buckets: int, max_key: int):
        if not math.log(buckets, 2).is_integer():
            raise Exception("Number of buckets needs to be power of two.")
        self.utility = Utility()
        self.counter_array = [0 for _ in range(buckets)]
        self.hash_bucket_funcs = self.utility.universal_hash_functions(max_key, 1,
                                                                       self.utility.primes_rsa)

    def update(self, key: int):
        """
        Basic primitive for adding to count array.

        Parameters:
            - key (int): item in stream to process.

        Return:
            - None
        """
        split = int(math.log(len(self.counter_array), 2))
        hash_value_bits = format(self.hash_bucket_funcs(key, 0,
                                                        2**64), "#066b")
        bucket = int(hash_value_bits[2:split+2], 2)
        bits = hash_value_bits[2+split:]
        msb = 64
        for idx, bit in enumerate(list(bits)):
            if int(bit):
                msb = idx+1
                break
        self.counter_array[bucket] = max(msb, self.counter_array[bucket])

    def estimate(self):
        """
        Basic primitive for estimating value in current count array. The
        value returns count distinct estimate.

        Parameters:
            - key (int): item for which estimate frequency.

        Return:
            - estimate (float): Estimation using hyper log log algorithm.
        """
        def derive(counter_size):
            constant = 0
            if counter_size <= 16:
                constant = 0.673
            elif counter_size <= 32:
                constant = 0.697
            elif counter_size <= 64:
                constant = 0.709
            elif counter_size >= 128:
                constant = 0.7213/(1+1.079/counter_size)
            return constant

        mean = sum([2**(-count) for count in self.counter_array])**-1
        return derive(len(self.counter_array))*(len(self.counter_array)**2)*mean

    def correct(self, estimate):
        """
        A routine for correcting an estimate in the case where it exceeds
        the size of the counter array by some ratio.

        More details can be found here: https://en.wikipedia.org/wiki/HyperLogLog

        Parameters:
            - key (int): item in stream to process.

        Return:
            - None
        """
        if estimate >= (5/2)*(len(self.counter_array)):
            raise Exception("Estimation larger than 5/2*(number of buckets).")
        zero_buckets = 0
        for count in self.counter_array:
            if not count:
                zero_buckets += 1
        if not zero_buckets:
            raise Exception("Estimation has sufficent number of buckets filled.")
        return len(self.counter_array)*\
            math.log(len(self.counter_array)/zero_buckets, 2)
