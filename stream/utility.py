"""
This is a module for count sketch primitive.
"""

import math
import random
from typing import List, Callable
from inspect import signature
from Crypto.PublicKey import RSA

class Utility:
    """
    Utility class contains helper methods that are likely
    to be used across probabilistic data structures.

    Return:
        - Instance of Utility.
    """
    def __init__(self):
        pass

    @staticmethod
    def primes(domain_min: int, domain_max: int,
               number_of_primes: int) -> List[int]:
        """
        This method generates "small" primes which are sufficent
        for universal hash functions. We do it the standard naive way. We
        loop over numbers in a domain to see if they are prime avoiding odd numbers.
        In the inner loop we only check up to the squre root of a number.
        We will either find all primes in a domain or the number of primes
        specified.

        Parameters:
            - domain_min (int): lower end of domain.
            - domain_max (int): upper end of domain.
            - number_of_primes (int): number of primes to find.

        Return:
            - list of found primes (List[int]).
        """
        primes = []
        if not domain_min % 2:
            domain_min += 1
        for possible in range(domain_min, domain_max, 2):
            if all(possible % check != 0 \
                   for check in range(3, int(math.sqrt(possible)+1))):
                primes.append(possible)
            if len(primes) == number_of_primes:
                break
        return primes

    @staticmethod
    def primes_rsa(domain_min: int,
                   number_of_primes: int) -> List[int]:
        """
        This method generates large primes using standard library
        RSA module. We specify a bit size for the key which has to
        be 1024 bits or larger as well as a multiple of 256. We also
        specify how many of these primes we want to generate.

        Parameters:
            - domain_min (int): number of bits in prime number.
            - number_of_primes (int): number of primes to find.

        Return:
            - list of found primes (List[int]).
        """
        primes = []
        for _ in range(number_of_primes):
            primes.append(RSA.generate(domain_min).p)
        return primes

    @staticmethod
    def universal_hash_functions(max_input: int,
                                 number_of_functions: int,
                                 prime_generator: Callable) -> Callable:
        """
        Generate a famuly of of universal hash functions that follow
        the general form of:

        ((a*x + b) % p) % b

        where 1 < a < p and is odd
        where 0 < b < p
        p is large prime more than max(x)

        Parameters:
            - max_input (int): maximum expected size of key.
            - number_of_functions (int): number of hash functions to generate.
            - prime_generator (Callable): generator function for primes.

        Return:
            - family of hash functions (Callable).
        """
        primes = None
        if len(signature(prime_generator).parameters) == 3:
            primes = prime_generator(max_input,
                                     int(max_input+1e6),
                                     number_of_functions)
        else:
            primes = prime_generator(max_input,
                                     number_of_functions)

        constants = [(random.randrange(1, prime-1, 2),
                      random.randrange(0, prime-1)) for prime in primes]
        return lambda x, i, b: ((constants[i][0] * x +
                                 constants[i][1]) % primes[i]) % b

    @staticmethod
    def sign_hash_functions(hash_function: Callable) -> Callable:
        """
        Map a family of of universal hash functions, that follow
        the general form, to hash function that is between -1 and 1.

        Parameters:
            - hash_function (Callabe): family of hash functions.

        Return:
            - family of hash functions that hash to -1 or 1 (Callable).
        """
        return lambda x, i: 1 if hash_function(x, i, 2) == 1 else -1
