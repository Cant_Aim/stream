"""
This is a module for count sketch primitive.
"""

import math
from stream.utility import Utility

class BloomFilter:
    """
    A bloom filter class that instantiates the necessary counting array,
    hash functions for assigning input to array indices. This is a modified
    version of bloom filter that allows for deletion as well as addition.

    .. code-block:: text

        *------------------*     A family hash functions assigns a given input
        |  |  |  |  |  |   |     to different indices. At provided indice we will
        *------------------*     increment the counter. For a deletion we decrement
                                 the counter.

    Parameters:
        - buckets (int): number of buckets to hash to.
        - hashs (int): number of hashing iterations to do.
        - max key (int): maximum expected size for a key.

    Return:
        - Instance of BloomFilter.
    """
    def __init__(self, buckets: int, hashs: int, max_key: int):
        self.hashs = hashs
        self.utility = Utility()
        self.counter_array = [0 for _ in range(buckets)]
        self.hash_bucket_funcs = self.utility.universal_hash_functions(max_key,
                                                                       hashs,
                                                                       self.utility.primes_rsa)

    def update(self, key: int, value: int = 1):
        """
        Basic primitive for adding to count array.

        Parameters:
            - key (int): item in stream to process.
            - value (int): multiplicative value associated to key (negatives remove)

        Return:
            - None.
        """
        buckets = set()
        for i in range(self.hashs):
            bucket = self.hash_bucket_funcs(key, i, len(self.counter_array))
            if bucket not in buckets:
                self.counter_array[bucket] += value
            buckets.add(bucket)

    def estimate(self, key: int):
        """
        Basic primitive for estimating value in current count array. The
        value returned is garuanteed lower bound on the amount of key elements
        stored.

        Parameters:
            - key (int): item for which estimate frequency.

        Return:
            - None.
        """
        estimate = math.inf
        for i in range(self.hashs):
            bucket = self.hash_bucket_funcs(key, i, len(self.counter_array))
            if self.counter_array[bucket] < estimate:
                estimate = self.counter_array[bucket]
        return estimate
