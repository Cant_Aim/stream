# Stream

[![pipeline status](https://gitlab.com/Cant_Aim/stream/badges/master/pipeline.svg)](https://gitlab.com/Cant_Aim/stream/commits/master)
[![coverage report](https://gitlab.com/Cant_Aim/stream/badges/master/coverage.svg)](https://gitlab.com/Cant_Aim/stream/commits/master)

## Introduction

A library for probabilistic data structures that are used in streaming literature to analyze large data streams. The data structures we will provide here include the following:

* Count-Sketch
* HyperLogLog _(WIP)_
* Bloom Filter _(WIP)_
* etc

This library is currently being developed but you are free to try and install it in it's current state.

## User Install

This package hasn't been pushed _pypi_ yet so you cannot simply run `pip install`. Once finished you will be able to do so.

## Developer Install

These instructions should work for default Python installations on Mac and Linux, not sure how they might work on Windows.
 
* Clone repository locally.

```bash
git clone git@gitlab.com:Cant_Aim/stream.git
```

* Create a virtual environment.

```bash
python3 -m venv stream-env
```

* Activate the environment in the `stream-env/bin` directory:

```bash
activate .
```

* Go to the directory with the `setup.py` file and run.

```bash
pip install -e .
```

## Documentation

Each module has in-code docstrings that can be accessed using emacs or the python interpreter. To get detail on a method or class do the following.

* Start python interpreter.
* Import the module you wish to learn about and run `help` on it.

```python
from stream.count_sketch import CountSketch
help(CountSketch)
``` 

Additionally for each release the documentation page will be updated [here](https://cant_aim.gitlab.io/stream/).

## Use

This demonstrates how to use various data structures.

### Count Sketch

Estimates the frequency of item occurances in a large stream. Initialize the stream with the number of buckets to hash to and the number of iterations hashing you want to do. The data structure offers probablistic gaurantees even in the worse case scenario where all items are equally like and relatively high degree of accuracy when the head is far heavier than the tail. Add a key item to the "sketch" as well as multiplicative factor to the counter if you wish. At any point during the stream you can estimate the occurances of an item.

```python
from stream.count_sketch import CountSketch

sketch = CountSketch(100, 100, 1000)

sketch.update(2, 1)
sketch.update(3, 1)
sketch.update(2, 1)
sketch.estimate(2)
sketch.estimate(3)
```

## Test

Go to the directory of the project and go into `tests` and run.

```bash
pip install '.[test]'
pytest
```

To run with coverage reporting execute the following:

```bash
coverage run -m pytest
coverage report 
coverage html
```
